package StringsDigitsMath;

import java.util.*;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.partitioningBy;

//подсчет гласных и согласных в строке

public class CountVowelsAndConsonants {

	private static final Set<Character> allVowels = new HashSet(Arrays.asList('a', 'e', 'o', 'i', 'u'));
	
		public static void main(String[] args) {
		System.out.println(Arrays.toString(countVowelAndConsonants("Hello World")));
		System.out.println(Arrays.toString(countVowelAndConsonantsByStream("Hello World")));
		System.out.println((countVowelAndConsonantsByCollector("Hello World")).toString());
	}
	
	public static int[] countVowelAndConsonants(String input) {
	    input = input.toLowerCase();
	    int vowel = 0;
	    int consonants = 0;
	    
	    for(int i = 0; i < input.length(); i++) {
	        char ch = input.charAt(i);
	        if(allVowels.contains(ch)) {
	            vowel++;
	        } else if ((ch >= 'a') && (ch <= 'z')) {
	            consonants++;
	        }
	    }
	    return new int[]{vowel, consonants};
	}
	
	public static long[] countVowelAndConsonantsByStream(String input) {
	    input = input.toLowerCase();
	    long vowels = input.chars()
	               .filter(character -> allVowels.contains((char)character))
	               .count();
	    long consonants = input.chars()
	               .filter(character -> !allVowels.contains((char)character))
	               .filter(character -> (character >= 'a') && (character <= 'z'))
	               .count();
	               
	   return new long[]{vowels, consonants};
	}

	public static Map<Boolean, Long> countVowelAndConsonantsByCollector(String input) {
	    Map<Boolean, Long> result = input.chars()
	                                .mapToObj(character -> (char)character)
	                                .filter(character -> (character >= 'a') && (character <= 'z'))
	                                .collect(partitioningBy(character -> allVowels.contains(character), counting()));
	                                
	   return result;
	}
}
