package StringsDigitsMath;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

//Инвертирование букв и слов в строке

public class ReverseWords {

	public static void main(String[] args) {
		String line = "New unique string for test realization";
		System.out.println(reverseWords(line));
		System.out.println(reverse(line));
		System.out.println(reverseByStream(line));
	}
	
	//разворачивает буквы, порядок слов остается
	public static String reverseWords(String input) {
		String WHITESPACE = " ";
		String[] words = input.split(WHITESPACE);
		StringBuilder reversedString = new StringBuilder();
		
		for(String word : words) {
			StringBuilder reversedWord = new StringBuilder();
			
			for(int i = word.length() - 1; i >= 0; i--) {
				reversedWord.append(word.charAt(i));
			}
			
			reversedString.append(reversedWord).append(WHITESPACE);
		}
		
		return reversedString.toString();
	}
	
	//инвертирует слова и буквы
	public static String reverse(String str) {
		return new StringBuilder(str).reverse().toString();
	}
	//инвертирования с помощью стримов и регулярных выражений
	public static String reverseByStream(String str) {
		Pattern PATTERN = Pattern.compile(" +");
		
		return PATTERN.splitAsStream(str)
				.map(word -> new StringBuilder(word).reverse())
				.collect(Collectors.joining(" "));
	}
}
