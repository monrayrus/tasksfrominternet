package StringsDigitsMath;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

//Удаление заданных символов
public class RemoveCharacters
{
	public static void main(String[] args) {
		System.out.println(removeCharacterByBuilder("Hello World", 'l'));
	}
	
	//Pattern.quote необходим для экранирования специальных символов
	public static String removeCharacter(String input, char ch) {
	    return input.replaceAll(Pattern.quote(String.valueOf(ch)), "");
	}
	
	public static String removeCharacterByBuilder(String input, char ch) {
	    StringBuilder sb = new StringBuilder();
	    char[] charArray = input.toCharArray();
	    
        for(char c : charArray) {
            if(c != ch) sb.append(c);
        }
	    
	    return sb.toString();
	}
	
	
	public static String removeCharacterByStream(String input, char ch) {
	    return input.chars().filter(c -> c != ch)
	                        .mapToObj(c -> String.valueOf((char)c))
	                        .collect(Collectors.joining());
	}
	
	//если нужно удалять суррогатную пару, то можно использовать codePointAt(), codePoints()
	public static String removeCharacterByStreamCP(String str, String ch) {
        int codePoint = ch.codePointAt(0);
        return str.codePoints()
                                .filter(c -> c != codePoint)
                                .mapToObj (c -> String.valueOf(Character.toChars(c)))
                                .collect (Collectors.joining());
	}
}
