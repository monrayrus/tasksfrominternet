package StringsDigitsMath;

public class CheckContainsOnlyDigits
{
	public static void main(String[] args) {
		System.out.println(containsOnlyDigits("Hello World"));
		System.out.println(containsOnlyDigitsByStream("123456"));
	}
	
	public static boolean containsOnlyDigits(String input) {
	    for(int i = 0; i < input.length(); i++) {
	        if(!Character.isDigit(input.charAt(i))) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static boolean containsOnlyDigitsByStream(String input) {
	    return !input.chars().anyMatch(number -> !Character.isDigit(number));
	}
	
	public static boolean containsOnlyDigitsByMatches(String input) {
	    return input.matches("[0-9]+");
	}
}
