package StringsDigitsMath;

//Отыскать символ с наибольшим числом появлений

//TODO проверить импорт


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class FindMaxOccurenciesCharacter
{
	public static void main(String[] args) {
		findSymbol("Hello World");
	}
	
	public static void findSymbol(String input) {
	    Map<Character, Integer> map = new HashMap<>();
	    char[] charArray = input.toCharArray();
	    
	    for(int i = 0; i < input.length(); i++) {
	        char currentChar = charArray[i];
	        if(!Character.isWhitespace(currentChar)) { //игнорирование пробелов
	            Integer num = map.get(currentChar);
	            if(num == null) {
	                map.put(currentChar, 1);
	            } else {
	                map.put(currentChar, ++num);
	            }
	        }
	    }
	    
	    int maxOccurences = Collections.max(map.values());
	    char maxChar = Character.MIN_VALUE;
	    
	    for(Map.Entry<Character, Integer> entry : map.entrySet()) {
	        if(entry.getValue() == maxOccurences) {
	            maxChar = entry.getKey();
	        }
	    }
	    
	    System.out.println(maxChar + ": " + maxOccurences);
	}
	//pair из пакета Apache
	public static Pair<Character, Long> findSymbolByStream(String input) {
	    return input.chars()
	                .filter(c -> Character.isWhitespace(c) == false)
	                .mapToObJ(c -> (char)c)
	                .collect(groupingBy(c -> c, counting()))
	                .entrySet()
	                .stream()
	                .max(comparingByValue());
	                map(p -> Pair.of(p.getKey(), p.getValue()))
	                .orElse(Pair.of(Character.MIN_VALUE, -1L));
	}
}
