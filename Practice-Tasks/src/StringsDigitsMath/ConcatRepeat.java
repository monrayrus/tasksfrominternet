//Конкатенирование одной и той же строки N раз

public class ConcatRepeat
{
	public static void main(String[] args) {
		System.out.println(concatRepeat("Hello ", 5));
		//Начиная с JDK 11 можно использовать repeat
		String result = "hello".repeat(5);
		
		System.out.println(isRepeated(result));
		System.out.println(isRepeated("helloolleh"));
	}
	
	public static String concatRepeat(String input, int n) {
	    StringBuilder sb = new StringBuilder(input.length() * n);
	    for(int i = 1; i <= n; i++) {
	        sb.append(input);
	    }
	    return sb.toString();
	}
	
	//проверка, является ли строка повторяющейся
	public static boolean isRepeated(String input) {
	    StringBuilder sb = new StringBuilder();
	    
	    for(int i = 0; i < input.length()/2; i++) {
	        sb.append(input.charAt(i));
	        String result = input.replaceAll(sb.toString(), "");
	        if(result.length() == 0) {
	            return true;
	        }
	    }
	    
	    return false;
	}
}
