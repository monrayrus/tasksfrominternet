package StringsDigitsMath;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

//поиск первого неповторяющегося символа в строке

public class FirstUniqueCharacter {
	
	//при использовании кодировки ASCII
	private final static int EXTENDED_ASCII_CODES = 256;
	
	public static void main(String[] args) {
		String line = "йnew unique string for test realization";
		System.out.println(findFirstUniqueChar(line));
		System.out.println(findFirstUniqueCharByLinkedHashMap(line));
		System.out.println(findFirstUniqueCharUnicode(line));
	}
	//при использовании кодировки ASCII, с кириллицей работать не будет если не изменять емкость кодировки
	public static char findFirstUniqueChar(String input) {
		int[] flags = new int[EXTENDED_ASCII_CODES];
		for(int i = 0; i < flags.length; i++) {
			flags[i] =-1;
		}
		
		for(int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			if(flags[ch] == -1) {
				flags[ch] = i;
			} else {
				flags[ch] = -2;
			}
		}
		
		int position = Integer.MAX_VALUE;
		for(int i = 0; i < EXTENDED_ASCII_CODES; i++) {
			if(flags[i] >= 0) {
				position = Math.min(position, flags[i]);
			}
		}
		
		return position == Integer.MAX_VALUE ? Character.MIN_VALUE : input.charAt(position);
	}
	
	//основано на том, что LinkedHashMap сохраняет порядок вставки элементов
	public static char findFirstUniqueCharByLinkedHashMap(String input) { 
		
		Map<Character, Integer> map = new LinkedHashMap<>();
		
		for(int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			map.compute(ch, (key, value) -> (value == null) ? 1 : ++value);
		}
		
		for(Map.Entry<Character, Integer> entry : map.entrySet()) {
			if(entry.getValue() == 1) {
				return entry.getKey();
			}
		}
		return Character.MIN_VALUE;
	}
	
	//поиск символов в юникоде
	public static String findFirstUniqueCharUnicode(String input) {
		Map<Integer, Long> map = input.codePoints()
				.mapToObj(codepoint -> codepoint)
				.collect(Collectors.groupingBy(Function.identity(),LinkedHashMap::new, Collectors.counting()));
		int codePoint = map.entrySet().stream()
				.filter(e -> e.getValue() == 1L)
				.findFirst()
				.map(Map.Entry::getKey)
				.orElse(Integer.valueOf(Character.MIN_VALUE));
		
		return String.valueOf(Character.toChars(codePoint));
	}

}
