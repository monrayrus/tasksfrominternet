/******************************************************************************

Применение отступа
Трансформирование строк
Вычисление минимума и максимума двух чисел
Сложение двух крупных чисел типа int/long и переполнение операции

*******************************************************************************/
import java.util.stream.*;
import java.util.*;
import java.util.function.*;

public class MultipleOperations
{
	public static void main(String[] args) {
		
		//Применение отступа с JDK 12
		List<String> days = Arrays.asList("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		for(int i = 0; i < days.size(); i++) {
		    System.out.print(days.get(i).indent(i));
		}
		
		//Трансформирование строк
		String transformResult = Stream.of("goool! ")
		                          .map(String::toUpperCase)
		                          .map(s -> s.repeat(2))
		                          .map(s -> s.replaceAll("O", "OOO"))
		                          .findFirst()
		                          .get();
		System.out.println(transformResult);
		//С JDK 12
		String transformHello = "hello".transform(s -> s + "world!");
		System.out.println(transformHello);
		
	    //Вычисление минимума и максимума двух чисел
	    float f1 = 33.34f;
	    float f2 = 33.213f;
	    float min = BinaryOperator.minBy(Float::compare).apply(f1, f2);
	    float max = BinaryOperator.maxBy(Float::compare).apply(f1, f2);
	    System.out.println( min + " " + max);
	}
	
	    //Сложение двух крупных чисел типа int/long и переполнение операции
	    int x = Integer.MAX_VALUE;
	    int y = Integer.MAX_VALUE;
	    int z = Math.addExact(x, y); // выбросит исключение ArithmeticException
	    BinaryOperator<Integer> operator = Math::addExact; //существуют методы multiplyExact(), substractExact(), negateExact(), incrementExact(), decrementExact()
	    int a = operator.apply(x, y); // выбросит исключение ArithmeticException
	
	
}
