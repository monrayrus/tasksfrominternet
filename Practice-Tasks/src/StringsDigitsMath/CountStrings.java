//подсчет числа появлений подстроки в строке
//вариант интерпретации (1) - 11 в 111 появляется 1 раз
//вариант интерпретации (2) - 11 в 111 появляется 2 раза
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class CountStrings
{
	public static void main(String[] args) {
		System.out.println(countStringFirstInterpretation("111", "11")); 
		System.out.println(countStringFirstInterpretation("Hello Hello Hello", "Hello")); 
		System.out.println(countStringSecondInterpretation("111", "11")); 
		System.out.println(countStringSecondInterpretation("Hello Hello Hello", "Hello"));
	}
	
    public static int countStringFirstInterpretation(String input, String toFind) {
        int position = 0;
        int count = 0;
        int n = toFind.length();
        
        while((position = input.indexOf(toFind, position)) != -1 ) {
            position += n;
            count++;
        }
        return count;
    } 
    
    public static int countStringSecondInterpretation(String input, String toFind) {
        Pattern pattern = Pattern.compile(Pattern.quote(toFind)); //регулярное выражение на основе toFind
        Matcher matcher = pattern.matcher(input);
        
        int position = 0;
        int count = 0;
        
        while(matcher.find(position)) {
            position = matcher.start() + 1;
            count++;
        }
        
        return count;
    }
	
}