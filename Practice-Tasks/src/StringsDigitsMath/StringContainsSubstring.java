//проверка наличия подстроки в строке
public class StringContainsSubstring
{
	public static void main(String[] args) {
		System.out.println(isContains("Hello World", "Help")); //false
		System.out.println(isContainsByCont("Hello World", "Hello")); //true
	}
	
	public static boolean isContains(String str, String substr) {
	    return str.indexOf(substr) != -1;
	}
	
		
	public static boolean isContainsByCont(String str, String substr) {
	    return str.contains(substr);
	}
	
}
