import java.util.stream.IntStream;

public class DeterminatePalindrome
{
	public static void main(String[] args) {
		System.out.println(isPalindrome("Hello World"));
		System.out.println(isPalindrome("ololo"));
	}
	
	public static boolean isPalindrome(String input) {
	    int num = input.length();
	    
	    for(int i = 0; i < num/2; i++) {
	        if(input.charAt(i) != input.charAt(num - i - 1)) {
	            return false;
	        }
	    }
	    return true;
	}
	
	public static boolean isPalindromeSecond(String input) {
	    int left = 0;
	    int right = input.length() - 1;
	    
	    while(right > left) {
	        if(input.charAt(left) != input.charAt(right)) {
	            return false;
	        }
	        left++;
	        right--;
	    }
	    
	    return true;
	}
	
	public static boolean isPalindromeByBuilder(String input) {
	    return input.equals(new StringBuilder(input).reverse().toString());
	}
	
	public static boolean isPalindromeStream(String input) {
	    return IntStream.range(0, input.length()/2)
	                    .noneMatch(word -> input.charAt(word) != input.charAt(input.length() - word - 1));
	}
}
