/******************************************************************************

Проверка строки, является ли палиндромом

*******************************************************************************/
public class isPalindromeClass
{
	public static void main(String[] args) {
		System.out.println(isPalindrome("Hello World"));
		System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
	}
	
    public static boolean isPalindrome (String s) {
        int i = 0;
        int j = s.length() - 1;
        
        while(i < j) {
            char st = s.charAt(i);
            char end = s.charAt(j);
            
            if(!Character.isLetterOrDigit(st)) {
                i++;
                continue;
            }
            
            if(!Character.isLetterOrDigit(end)) {
                j--;
                continue;
            }
            
            if(Character.toLowerCase(st) != (Character.toLowerCase(end))) return false;
            
            i++;
            j--;
        }
        
        return true;
    }
}
