/******************************************************************************

Поиск наибольшего общего префиска

*******************************************************************************/
public class LongestCommonPrefix
{
	public static void main(String[] args) {
	    String texts[] = {"abc", "abcd", "abcde", "ab", "abcd", "abcdef"};
		System.out.println(longestCommonPrefix(texts));
	}
	
	public static String longestCommonPrefix(String[] array) {
	    if(array.length == 1) {
	        return array[0];
	    }
	    
	    int firstLen = array[0].length();
	    
	    for(int prefixSize = 0; prefixSize < firstLen; prefixSize++) {
	        char ch = array[0].charAt(prefixSize);
	        for(int i = 1; i < array.length; i++) {
	            if(prefixSize >= array[i].length() || array[i].charAt(prefixSize) != ch) {
	                return array[i].substring(0, prefixSize);
	            } 
	        }
	    }
	    
	    return array[0];
	}
}
