package StringsDigitsMath;

import java.util.stream.IntStream;

//генерирование всех перестановок
public class GeneratePermutations
{
	public static void main(String[] args) {
		permuteAndPrint("ABC");
	}
	//нужно для того, чтобы изначально инициализировать строкой ""
	public static void permuteAndPrint(String input) {
	    permuteAndPrint("", input);
	}
	//здесь также можно использовать SET для того, чтобы хранить данные и избегать повторений
	public static void permuteAndPrint(String prefix, String input) {
	    int num = input.length();
	    
	    if(num == 0) {
	        System.out.print(prefix + " ");
	    } else {
	        for(int i = 0; i < num; i++) {
	          //0 цикл
	          //permuteAndPrint("" + 'A', "BC"); (A, BC)
	          //permuteAndPrint('A' + 'B', "C");(AB, C)
	          //permuteAndPrint("AB" + 'C', "" + "");(ABC,)
	            permuteAndPrint(prefix + input.charAt(i), input.substring(i + 1, num) + input.substring(0, i));
	            
	        }
	    }
	}
	
	private static void permuteAndPrintStream(String prefix, String input) {
	    int num = input.length();
	    
	    if(num ==0) {
	        System.out.println(prefix);
	    } else {
	        IntStream.range(0, num)
	        .parallel()
	        .forEach(i -> permuteAndPrintStream(prefix + input.charAt(i), input.substring(i + 1, num) + input.substring(0, i)));
	    }
	}
}
