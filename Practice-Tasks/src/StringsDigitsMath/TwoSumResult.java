/******************************************************************************

Найти индексы двух чисел из массива, образующие заданную сумму

*******************************************************************************/
import java.util.*;
public class TwoSumResult
{
	public static void main(String[] args) {
		
		int[] arr = new int[]{2,7,11,15};
		System.out.println(Arrays.toString(twoSum(arr, 9)));
	}
	
	public static int[] twoSum(int[] nums, int target) {
        int[] result = new int[]{0,0};
        for(int i = nums.length - 1; i > 0; i--) {
            for(int j = 0; j < i; j++) {
                if(nums[i] + nums[j] == target) {
                    return new int[]{j,i};
                }
            }
        }
        return result;
    }

	public static int[] twoSumTwo(int[] nums, int target) {
        HashMap<Integer, Integer> prevMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            int diff = target - num;

            if (prevMap.containsKey(diff)) {
                return new int[] { prevMap.get(diff), i };
            }

            prevMap.put(num, i);
        }

        return new int[] {};
    }
}
