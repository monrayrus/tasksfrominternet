//подсчет появлений некоторого символа
package StringsDigitsMath;

public class CountOccurrencesOfACertainCharacter
{
	public static void main(String[] args) {
		System.out.println(countOccurences("Hello World", 'l'));
		System.out.println(countOccurencesBySurrogates("Hello World", "l"));
	}
	
	//убрать все вхождения символа и вернуть разницу строк
	public static int countOccurences(String input, char ch) {
	    return input.length() - input.replace(String.valueOf(ch), "").length();
	}
	
	//проверка на наличие юникодовых суррогатных пар
	public static int countOccurencesBySurrogates(String input, String ch) {
	    if(ch.codePointCount(0, ch.length()) > 1 ) {
	        return -1;
	    }
	    
	    int result = input.length() - input.replace(ch, "").length();
	    return ch.length() == 2? result/2 : result;
 	}
 	
 	//обход циклом
 	public static int countOccurencesByCycle(String input, char ch) {
 	    int count = 0;
 	    
 	    for(int i = 0; i < input.length(); i++) {
 	        if(input.charAt(i)==ch) {
 	            count++;
 	        }
 	    }
 	    
 	    return count;
 	}
 	//через простой стрим
 	public static long countOccurencesByStream(String input, char ch) {
	    return input.chars()
	                .filter(character -> character == ch)
	                .count();
	}
 	
}
