import java.util.Arrays;
import java.util.Comparator;

//сортировка массива строк по длине
public class SortingStringArrayByLength
{
	public static void main(String[] args) {
		String[] arr = new String[]{"Help", "World", "Yo", "Tennessy"};
		sortArrayOfStringByLength(arr, true);
		System.out.println(Arrays.toString(arr));
	}
	
	
	//использование компаратора
	public static void sortArrayOfStringByLength(String[] array, boolean ascending) {
	    if(ascending) {
	        Arrays.sort(array, (String s1, String s2) -> Integer.compare(s1.length(), s2.length()));
	    } else {
	        Arrays.sort(array, (String s1, String s2) -> (-1) * Integer.compare(s1.length(), s2.length()));
	    }
	}
	
	
	public static String[] sortArrayOfStringByLengthByStream(String[] arr, boolean ascending) {
	    if(ascending) {
	        return Arrays.stream(arr)
	                     .sorted(Comparator.comparingInt(String::length))
	                     .toArray(String[]::new);
	    } else {
	        return Arrays.stream(arr)
	                     .sorted(Comparator.comparingInt(String::length).reversed())
	                     .toArray(String[]::new);
	    }
	}
	
}
