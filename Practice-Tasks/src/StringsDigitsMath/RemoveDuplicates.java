package StringsDigitsMath;

//Удаление повторяющихся символов
import java.util.*;
import java.util.stream.Collectors;

public class RemoveDuplicates
{
	public static void main(String[] args) {
		System.out.println(removeDuplicatesByHashSet("Hello World"));
	}
	
	public static String removeDuplicates(String input) {
	    char[] charArray = input.toCharArray();
	    StringBuilder sb = new StringBuilder();
	    for(char ch : charArray) {
	        if(sb.indexOf(String.valueOf(ch)) == -1) {
	            sb.append(ch);
	        }
	    }
	    
	    return sb.toString();
	}
	
	public static String removeDuplicatesByHashSet(String input) {
	    char[] charArray = input.toCharArray();
	    StringBuilder sb = new StringBuilder();
	    Set<Character> set = new HashSet<>();
	    
	    for(int i = 0; i < input.length(); i++) {
	        if(set.add(charArray[i])) {
	            sb.append(charArray[i]);
	        }
	    }
	    
	    return sb.toString();
	    }
	    
	public static String removeDuplicatesByStream(String input) {
	    return Arrays.asList(input.split("")).stream() //преобразует строку в стрим, где каждый символ является элементом стрима
	                                         .distinct() //удаляет из потока повторы
	                                         .collect(Collectors.joining());//конкатенируется в строку по порядку
	}
}
