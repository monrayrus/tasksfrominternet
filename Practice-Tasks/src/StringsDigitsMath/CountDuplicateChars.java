package StringsDigitsMath;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

//Подсчет повторяющихся символов

public class CountDuplicateChars {

	public static void main(String[] args) {
		String line = "New unique string for test realization";
		System.out.println(countDuplicatesByMap(line));
		System.out.println(countDuplicatesByStream(line));
		System.out.println(countDuplicatesSurrogatePairs(line));

	}
	
	public static Map<Character, Integer> countDuplicatesByMap(String input) {
		Map<Character, Integer> map = new HashMap<>();
		for(int i =0; i < input.length(); i++) {
			char ch = input.charAt(i);
			//использование бифункции для мапы, ch как ключ
			map.compute(ch, (key, value) -> (value == null) ? 1 : ++value);
		}
		
		return map;
	}
	
	public static Map<Character, Long> countDuplicatesByStream(String input) {
		Map<Character, Long> result = input.chars() //возвращает экземпляр IntStream, содержит целочисленное представление символов в строке
				.mapToObj(ch ->(char)ch) //переводит поток IntStream в поток символов
				.collect(Collectors.groupingBy(ch -> ch, Collectors.counting())); //группировка и подсчет символов
		return result;
	}
	
	//подсчет символов, которые состоят из суррогатной пары
	public static Map<String, Integer> countDuplicatesSurrogatePairs(String input) {
		Map<String, Integer> map = new HashMap<>();
		for(int i =0; i < input.length(); i++) {
			int cp = input.codePointAt(i);
			String ch = String.valueOf(Character.toChars(cp));
			if(Character.charCount(cp) == 2) {
				i++;
			}
			map.compute(ch, (key, value) -> (value == null) ? 1 : ++value);
		}
		
		return map;
	}

}
