package homeworks;

import java.util.Arrays;
import java.util.stream.*;


public class CodewarsTwoInOne {
	
//	Take 2 strings s1 and s2 including only letters from ato z. Return a new sorted string, the longest possible, containing distinct letters - each taken only once - coming from s1 or s2.
//
//	Examples:
//	a = "xyaabbbccccdefww"
//	b = "xxxxyyyyabklmopq"
//	longest(a, b) -> "abcdefklmopqwxy"
//
//	a = "abcdefghijklmnopqrstuvwxyz"
//	longest(a, a) -> "abcdefghijklmnopqrstuvwxyz"
	

	public static void main(String[] args) {
		
		System.out.println(longestMy("xyaabbbccccdefww", "xxxxyyyyabklmopq"));

	}

	public static String longestMy (String s1, String s2) {
		
		String concat = s1 + s2;
		char[] stringArray = concat.toCharArray();
		Arrays.sort(stringArray);
		StringBuilder builder = new StringBuilder();
		
		char temp = ' ';
		for(char symbol : stringArray) {
			if(symbol == temp) {
				continue;
			} else if (symbol != temp) {
				builder.append(symbol);
				temp = symbol;
			}
		}
		
		return builder.toString();
		
    }
	
    public static String longestOne (String s1, String s2) {
        String s = s1 + s2;
        return s.chars().distinct().sorted().collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
        
        //distinct() removes duplicates
    }
    
    public static String longestTwo (String s1, String s2) {
        StringBuilder sb = new StringBuilder();
        (s1 + s2).chars().distinct().sorted().forEach(c -> sb.append((char) c));
        return sb.toString();
    }
    
    public static String longestThree (String s1, String s2) {
        return Stream.of(s1.concat(s2).split(""))
                  .sorted()
                  .distinct()
                  .collect(Collectors.joining());
        
        //java.util.stream.Collectors.joining() is the most simple joining method which does not take any parameter. 
        //It returns a Collector that joins or concatenates the input streams into String in the order of there appearance.
    }
}
