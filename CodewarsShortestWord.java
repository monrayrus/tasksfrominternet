package homeworks;

//Simple, given a string of words, return the length of the shortest word(s).

//String will never be empty and you do not need to account for different data types.

import java.util.stream.*;
import java.util.Arrays;
import java.util.Comparator;

public class CodewarsShortestWord {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	 public static int findShortMy(String s) {
	        String[] array = s.split(" ");
	        int num = array[0].length();
	        
	        for(int i = 1; i < array.length; i++) {
	        	if(num > array[i].length()) {
	        		num = array[i].length();
	        	}
	        }
	        
	        return num;
	    }
	 
	 public static int findShortOne(String s) {
		 return Arrays.stream(s.split(" ")).sorted(Comparator.comparing(String::length)).findFirst().get().length();
	    }
	 
	 
	 public static int findShortTwo(String s) {
	        return Stream.of(s.split(" "))
	          .mapToInt(String::length)  //преобразует каждый элемент в инт
	          .min()
	          .getAsInt();
	    }
	

}
